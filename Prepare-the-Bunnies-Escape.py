'''
Prepare the Bunnies' Escape===========================

You're awfully close to destroying the LAMBCHOP doomsday device and freeing Commander Lambda's bunny workers, but once they're free of the work duties the bunnies are going to need to escape Lambda's space station via the escape pods as quickly as possible. Unfortunately, the halls of the space station are a maze of corridors and dead ends that will be a deathtrap for the escaping bunnies. Fortunately, Commander Lambda has put you in charge of a remodeling project that will give you the opportunity to make things a little easier for the bunnies. Unfortunately (again), you can't just remove all obstacles between the bunnies and the escape pods - at most you can remove one wall per escape pod path, both to maintain structural integrity of the station and to avoid arousing Commander Lambda's suspicions. You have maps of parts of the space station, each starting at a work area exit and ending at the door to an escape pod. The map is represented as a matrix of 0s and 1s, where 0s are passable space and 1s are impassable walls. The door out of the station is at the top left (0,0) and the door into an escape pod is at the bottom right (w-1,h-1). Write a function solution(map) that generates the length of the shortest path from the station door to the escape pod, where you are allowed to remove one wall as part of your remodeling plans. The path length is the total number of nodes you pass through, counting both the entrance and exit nodes. The starting and ending positions are always passable (0). The map will always be solvable, though you may or may not need to remove a wall. The height and width of the map can be from 2 to 20. Moves can only be made in cardinal directions; no diagonal moves are allowed.

Languages=========
To provide a Python solution, edit solution.py
To provide a Java solution, edit Solution.java

Test cases==========
Your code should pass the following test cases.Note that it may also be run against hidden test cases not shown here.

-- Python cases --
Input:solution.solution([[0, 0, 0, 0, 0, 0], [1, 1, 1, 1, 1, 0], [0, 0, 0, 0, 0, 0], [0, 1, 1, 1, 1, 1], [0, 1, 1, 1, 1, 1], [0, 0, 0, 0, 0, 0]])Output:    11

Input:solution.solution([[0, 1, 1, 0], [0, 0, 0, 1], [1, 1, 0, 0], [1, 1, 1, 0]])Output:    7
-- Java cases --
Input:Solution.solution({{0, 1, 1, 0}, {0, 0, 0, 1}, {1, 1, 0, 0}, {1, 1, 1, 0}})Output:    7

Input:Solution.solution({{0, 0, 0, 0, 0, 0}, {1, 1, 1, 1, 1, 0}, {0, 0, 0, 0, 0, 0}, {0, 1, 1, 1, 1, 1}, {0, 1, 1, 1, 1, 1}, {0, 0, 0, 0, 0, 0}})Output:    11


'''


from collections import deque

def solution(m):
    def isValid(mat, visited, row, col):
        if 0 <= row < M and 0 <= col < N and mat[row][col] == 0 and (not visited[row][col]):
            return True
        else:
            return False
        #return (True if 0<=row<M and 0<=col<N and mat[row][col] == 0 and (not visited[row][col]) else False)

    def findTheWay(mat, src, dest):
        ways = [0, 1], [1, 0], [0, -1], [-1, 0]
        i, j = src
        x, y = dest
        visited = []
        for _ in range(M):
            visited.append([None] * N)
        #visited = [[False for x in range(N)] for y in range(M)]
        q = deque()
        visited[i][j] = True
        q.append((i, j, 0))
        min_dist = M*N
        while q:
            (i, j, dist) = q.popleft()
            if i == x and j == y:
                min_dist = dist
                break
            for dX, dY in ways:
                newX = i + dX
                newY = j + dY
                if isValid(mat, visited, newX, newY):
                    visited[newX][newY] = True
                    q.append((newX, newY, dist + 1))
        if min_dist != minWay:
            return min_dist
        else:
            return -1
    M = len(m)
    N = len(m[0])
    minWay = 20*20-1
    for x in range(0,M):
        for y in range(0,N):
            tf = findTheWay(m, (x, y), (0, 0))
            ts = findTheWay(m, (x, y), (M-1, N-1))
            if tf>0 and ts>0:
                minWay = min(minWay, tf+ts)
    return minWay+1


maze1 = [
    [0, 0, 0, 0, 0, 0],
    [1, 1, 1, 1, 1, 0],
    [0, 0, 0, 0, 0, 0],
    [0, 1, 1, 1, 1, 1],
    [0, 1, 1, 1, 1, 1],
    [0, 0, 0, 0, 0, 0]]
# )Output:    11
maze2 = [
    [0, 1, 1, 0],
    [0, 0, 0, 1],
    [1, 1, 0, 0],
    [1, 1, 1, 0]]

#Output:    7


print(solution(maze1), 11)
print(solution(maze2), 7)

