def solution(xs):
    belowZero = 0
    zeroCount = 0
    aboveZero = 0
    maxMin = -999999

    for t in xs:
        if t<0:
            belowZero += 1
            maxMin = max(t, maxMin)
        if t == 0:
            zeroCount +=1
        if t>0:
            aboveZero +=1
    if belowZero == 0 and aboveZero == 0:
        return str(0)
    if aboveZero == 0 and belowZero == 1:
        if zeroCount>0:
            return str(0)
        return str(maxMin)


    result = 1
    for r in xs:
        if r != 0:
            result = result * r
    if belowZero%2 != 0:
        result = result/maxMin
        return str(int(result))


    return str(result)

print(solution([2, 0, 2, 2, 0]))
print(solution([-2, -3, 4, -5]))
print(solution([1,0,-1,-2]))
print(solution([-2,0]))
print(solution([2,-3,1,0,-5]))
print(solution([-3]))
