from math import atan2, sqrt

def solution(roomSize, src, dst, power):
    cases = 0
    roomX, roomY = roomSize
    myX, myY = src
    #print(power, myX, myY, roomX, roomY)
    roomAboveX = (myY + power) // roomY + 1
    roomUnderX = (power - myY) // roomY + 1
    roomAboveY = (myX + power) // roomX + 1
    roomUnderY = (power - myX) // roomX + 1

    cx = power // roomX+1
    cy = power // roomY+1
    shoots = {}
    possibilites = [(1, 1), (-1, 1), (-1, -1), (1, -1)]
    for rooms in ((x,y) for x in range(cx+1) for y in range(cy+1)):
        for cooridates in src, dst:
            for sides in possibilites:
                for a, b in [(zip(roomSize, cooridates, rooms, sides))]:
                    #print("a, b =",a,b)
                    x = (a[0] * a[2] + (a[0] - a[1] if a[2] % 2 else a[1])) * a[3]
                    y = (b[0] * b[2] + (b[0] - b[1] if b[2] % 2 else b[1])) * b[3]

                distance = sqrt((abs(x - myX) ** 2 + abs(y - myY) ** 2))
                angle = atan2(myX - x, myY - y)
                if distance > power or (angle in shoots):
                    continue
                #if distance > power or (angle in shoots and distance > abs(shoots[angle])):
                #    continue

                shoots[angle] = distance * (-1 if cooridates == src else 1)

    return len(list(filter(lambda x: (shoots[x] > 0), shoots)))





print(solution([300,275], [150,150], [185,100], 500))