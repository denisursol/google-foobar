import functools
def solution(start, length):
    def fxor(n):
        if n == 0:
            return 0
        if n & 3 == 0:
            return n
        elif n & 3 == 1:
            return 1
        elif n & 3 == 2:
            return n + 1
        elif n & 3 == 3:
            return 0

    result = 0
    #i = 0
    for i in range(0,length):
    #while i < length:
        v = start + i
        start = v + length - i - 1
        result ^= fxor(start) ^ fxor(v - 1)
    #    i += 1

    return result

print(solution(17,4))  # must be 14
print(solution(0,3))  # must be 2
#print(solution(1000400000,5))
